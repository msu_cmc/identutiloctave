% -*- texinfo -*-
%@deftypefn{Function File} {@var{name_list} = } sig_names(@var{SV}, @var{mask})
% Convert signal @var{mask} to list of signal names @var{name_list}.
%
%@seealso{parse_signal_desc, sig_mask}
%@end deftypefn
function name_list = sig_names (SV, mask)
	k = 0;
	for l = 1:numel(SV) 
		if (bitand(mask, bitset(0, l))) 
			k++;
			name_list{1, k} = SV{l}.name;
		end
	end
end

