% -*- texinfo -*-
%@deftypefn{Function File} {@var{SD} = } parse_signal_desc (@var{str})
% Parse signal (state variable) description line @var{str}.
%
%@seealso{IdentDevice}
%@end deftypefn
function SV = parse_signal_desc (str)
	% name
	[token, str] = strtok(str);
	SV.name = token;
	% units
	[token, str] = strtok(str);
	SV.units = token;
	% coeff
	[token, str] = strtok(str);
	token = str2num(token);
	SV.coeff = token;
	% SV.offset
	[token, str] = strtok(str);
	token = str2num(token);
	SV.offset = token;
	% min
	[token, str] = strtok(str);
	token = str2num(token);
	SV.min = token;
	% max
	[token, str] = strtok(str);
	token = str2num(token);
	SV.max = token;
	% range: saturation or continous
	[token, str] = strtok(str);
	SV.range = token;
	% type
	[token, str] = strtok(str);
	SV.type = token;

	disp(SV)

	if (isempty(SV.name) || isempty(SV.units) ||
			isempty(SV.coeff) || isempty(SV.offset) || isempty(SV.min) || isempty(SV.max) || 
					isempty(SV.range) || isempty(SV.type))
			error("IdentDevice: parse_signal_desc: unable parse string <%s>.", str);
	end
end

