% -*- texinfo -*-
%@deftypefn{Function File} {@var{mask} = } sig_mask(@var{SV}, @var{name_list})
% Convert list of signal names @var{name_list} to corresponding @var{mask}.
% if @var{name_list} is a mask then check correctnes.
%
%@seealso{parse_signal_desc}
%@end deftypefn
function mask = sig_mask (SV, name_list)
	if (isreal(name_list)) 
		mask = name_list;
		if (~isscalar(mask) || floor(mask) ~= mask || mask < 1 || mask > 2^numel(SV)) 
			error('IdentDevice: sig_mask: bad sidnal mask <0x%x>', mask);
		end
	else
		mask = 0;
		for k = 1:numel(name_list)
			name_found = false;
			for l = 1:numel(SV)
				if (strcmp(name_list{k}, SV{l}.name))
					mask = bitset(mask, l);
					name_found = true;
				end
			end
			if (~name_found) 
				error('IdentDevice: sig_mask: unknown signal name');
			end
		end
	end
end

