function out = recover_int16(in) 
	out = in;
	correction = 0;
	for k=2:length(in)
		dy = in(k) - in(k-1);
		if (dy > 2^15) 
			correction -= 2^16;
		elseif (dy < -2^15)
			correction += 2^16;
		end
		out(k) = out(k) + correction;
	end
end
