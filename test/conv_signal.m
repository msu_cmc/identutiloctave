% -*- texinfo -*-
%@deftypefn{Function File} {@var{SD} = } conv_signal (@var{SV}, sig_in, mask)
% Convert signal from units to sequence of uint16.
%
%@seealso{parse_signal_desc}
%@end deftypefn
function out_sig = conv_signal (SV, in_sig, mask)
	ind = 1;
	out_sig = [];
	for k = 1:numel(SV)
		if (bitand(mask, bitset(0, k))) 
			out = in_sig(:, ind);
			ind++;
			out = out / SV{k}.coeff + SV{k}.offset;
			switch SV{k}.range 
			case 'sat'
				if (any(out > SV{k}.max) || any(out < SV{k}.min))
					warning("IdentDevice: conv_signal: input signal is out of range");
				end
				out = max(out, SV{k}.min);
				out = min(out, SV{k}.max);
			case 'cont'
				out -= SV{k}.min;
				out = mod(out, SV{k}.max - SV{k}.min);
				out += SV{k}.min;
			end
			out_sig = horzcat(out_sig, out);
		end
	end
end

