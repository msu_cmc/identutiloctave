% -*- texinfo -*-
%@deftypefn{Function File} {@var{sig_out} = } iconv_signal (@var{SV}, sig_in, mask)
% Convert signal from sequence of uint16 to physical units.
%
%@seealso{parse_signal_desc, conv_signal}
%@end deftypefn
function out_sig = iconv_signal (SV, in_sig, mask)
	ind = 1;
	out_sig = [];
	for k = 1:numel(SV)
		if (bitand(mask, bitset(0, k))) 
			in = in_sig(:, ind);
			ind++;
			if (SV{k}.range == 'cont')
				out = in;
				sig_range = SV{k}.max - SV{k}.min;
				correction = 0;
				for l=2:length(in)
					dy = in(l) - in(l-1);
					if (dy > sig_range/2) 
						correction -= sig_range;
					elseif (dy < -sig_range/2)
						correction += sig_range;
					end
					out(l) += correction;
				end
			end
			out = SV{k}.coeff * (out - SV{k}.offset);
			out_sig = horzcat(out_sig, out);
		end
	end
end

