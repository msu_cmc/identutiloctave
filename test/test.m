SV{1} = parse_signal_desc("position pulse 1 0 -32768 32767 cont sink");
SV{2} = parse_signal_desc("speed pulse/s 0.01 0  -inf +inf sat none");
SV{3} = parse_signal_desc("current A 0.00572917 0 -inf +inf sat none");


mask = 0x2
name_list = sig_names(SV, mask)
mask = 0x5
name_list = sig_names(SV, mask)

mask 
mask = sig_mask(SV, mask)

name_list = { 'position', 'speed' }
mask = sig_mask(SV, name_list)

in = linspace(0, 100000, 1000010)';
plot(in,'-;in;b');
hold on
out = conv_signal(SV, in, 1);
plot(out,'-;in;r');
in = iconv_signal(SV, out, 1);
plot(in,'-;in_{recovered};g');
hold off

