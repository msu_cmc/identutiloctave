% Open output file
files = {
	'@IdentDevice/IdentDevice.m';
	'@IdentDevice/command.m';
	'@IdentDevice/conv_signal.m';
	'@IdentDevice/delete.m';
	'@IdentDevice/describe.m';
	'@IdentDevice/feedback.m';
	'@IdentDevice/flush.m';
	'@IdentDevice/get.m';
	'@IdentDevice/get_signal_conv.m';
	'@IdentDevice/iconv_signal.m';
	'@IdentDevice/inputexpanded.m';
	'@IdentDevice/recv.m';
	'@IdentDevice/repeatexp.m';
	'@IdentDevice/response.m';
	'@IdentDevice/responseperiodic.m';
	'@IdentDevice/send.m';
	'@IdentDevice/set.m';
	'@IdentDevice/set_signal_conv.m';
	'@IdentDevice/setperiod.m';
};

fid = fopen('doc/functions.texi', 'w');

for k=1:rows(files)
	file = files{k};
	[TEXT, FORMAT] = get_help_text(file);
	
    fputs(fid, TEXT);
end

fclose(fid);
