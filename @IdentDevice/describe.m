% -*- texinfo -*-
%@deftypefn{Function File} {} describe (@var{ID})
%Prints experiment and signal description in the string format (see @code{set_signal_conv}).
%
%@seealso{IdentDevice, set_signal_conv, get_signal_conv}
%@end deftypefn
function describe(ID)
	send(ID, 'dex');
	disp(getline(ID));
	disp(getline(ID));
	recv(ID, 'OK');
	printf('\nN\tSIGNAL_NAME UNITS\n');
	for k = 0 : (ID.state_dim-1)
		send(ID, 'dsv %d', k);
		str = getline(ID);
		recv(ID, 'OK');
		printf('%d\t%s\n', k, str);
	end	
end

