function out=ldr(ID, pos, len)

    send(ID, 'ldr %d %d', pos, len);
	out=readoutput(ID);
	recv(ID, 'OK');

end
