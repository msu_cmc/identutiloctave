% -*- texinfo -*-
%@deftypefn{Function File} { @var{Out} = } repeatexp (@var{ID})
%Repeat the previous experiment with target device @var{ID}.
%
%This action is possible because input vector remains in device memory if successful experiment has been performed 
%with @code{response} or @code{responseperiodic} function call.
%
%@seealso{IdentDevice, response, responseperiodic}
% @end deftypefn
function Out = repeatexp(ID)
	% Конфигурируем вход 
	if (ID.success) 
		error('IdentDevice: cannon repeat unexisting experiment')
	end	
	% Проводим эксперимент
	send(ID, 'exp');
	Out = readoutput(ID);
	recv(ID, 'OK');
	% convert output signal
	Out = iconv_signal(ID, Out, ID.sink_mask);
end
