% -*- texinfo -*-
%@deftypefn {Function File} {@var{ID} = } set (@var{ID},@var{prop1},@var{val1},@var{prop2},@var{val2}...)
%Set property @var{PROP} to value @var{VAL}. @var{ID} is device running ident-util.
%
%Available properties:
%@table @samp
%@item input_div
%    Input divider. The input of plant will be updated only on each @var{input_div} cycle.
%    For example if input divider is 3 and input vector is @code{[1 2 3]}, 
%    then real plant input is @code{[1 1 1 2 2 2 3 3 3]}.    
%    
%    Default value: 1.
%@item output_div
%    Output divider. Output passed to Octave only if cycle number is divisible by @var{output_div}.
%    For example, if plant output is @code{[1 2 3 4 5]} with @var{output_div} equal 2 we get 
%    vector @code{[1 NaN 3 NaN 5]}.
%    
%    Default value: 1.
%@item output_delay
%    Wait for @var{output_delay} cycles before starting acquire plant output. 
%    For example, if plant output is @code{[1 2 3 4 5]} with @var{output_delay} equal 2 we get 
%    vector @code{[NaN NaN 3 4 5]}.
%    
%    Default value: 0.
%@item output_n
%    Number of samples to acquire from the plant. If set to @code{[]} duration (number of cycles)
%    is computed from length of input. Duration of experiment is
%    @example
%@var{ncycles} = MAX( @var{output_delay} + @var{output_div}*@var{output_n}, @var{input_div}*length(@var{INPUT}) )
%    @end example
%    Default value: 0.
%@item T
%    Set sample rate. @var{T} is period in seconds.
%
%    Default value: 0.01 s.
%@item io_mode
%    Set data transfer I/O mode. Possible values:
%    @table @samp
%    @item decimal
%    	Use human-readable decimal numbers. It takes approximately 1.2 ms to transmit one 16-bit integer on baurdrate 57600. 
%    @item decimal
%    	Use raw (little-endian) format. It takes approximately 0.4 ms to transmit one 16-bit integer on baurdrate 57600. 
%		Error on device backend during experiment may cause a hang of Octave.
%    @end table
%@item output_mask, output_names
%    Set @code{sink_mask} of the target. Select output signals of the target device form its signal buffer.
%    Mask is an integer (binary mask) or the cell array of signal names.
%
%    Default value: depends on device.
%@item input_mask, input_names
%    Set @code{source_mask} of the target. Select input signals of the target device form its signal buffer.
%    Mask is an integer or the cell array of signal names.
%
%    Default value: depends on device.
%@item fb_ref_mask, fb_ref_names
%    Set @code{reference_mask} for feedback. 
%    Mask is an integer or the cell array of signal names.
%
%    Default value: depends on device.
%@item fb_in_mask, fb_in_names
%    Set input signal mask for feedback
%    Mask is an integer or the cell array of signal names.
%
%    Default value: depends on device.
%@item fb_out_mask, fb_out_names, control_mask, control_names
%    Set output signal mask for feedback. 
%    Mask is an integer or the cell array of signal names.
%
%    Default value: depends on device.
%@item timeout
%    Serial port opertion timeout (seconds).
%
%    Default value: 5.0
%@end table
%
%@seealso{IdentDevice, get} 
%@end deftypefn
function out = set(ID, varargin)
	if (length (varargin) < 2 || rem (length (varargin), 2) ~= 0)
		error ('IdentDevice: set: expecting property/value pairs');
	end
	while (length(varargin) > 1)
		prop = varargin{1};
		val = varargin{2};
		varargin(1:2) = [];
		if (~ischar(prop))
			error ('IdentDevice: set: invalid property of class');
		end
		if (regexp(prop, 'input_div|output_div|output_delay'))
		   	if (isscalar(val) && isreal(val) && val > 0 && floor(val) == val)
				switch prop
				case 'input_div'
					ID.source_div = val;
				case 'output_div'
					ID.sink_div = val;
				case 'output_delay'
					ID.sink_delay = val;
				end
			else
				error ('IdentDevice: set: %s : positive scalar integer is expected.', prop);
			end
		elseif (regexp(prop,'(input|output|fb_ref|fb_in|fb_out|control)_(mask|names)')) 
			if (isreal(val) || iscell(val))
				switch (prop)
				case {'input_mask', 'input_names'}
					ID.source_mask = sig_mask(ID.signal_desc, val);
				case {'output_mask', 'output_names'}
					ID.sink_mask = sig_mask(ID.signal_desc, val);
				case {'fb_ref_mask', 'fb_ref_names'}
					ID.fb_ref_mask = sig_mask(ID.signal_desc, val);
				case {'fb_in_mask', 'fb_in_names'}
					ID.fb_in_mask = sig_mask(ID.signal_desc, val);
				case {'fb_out_mask', 'fb_out_names', 'control_mask', 'control_names'}
					ID.control_mask = sig_mask(ID.signal_desc, val);
				end
			else
				error('IdentDevice: set: %s : scalar integer or cell array of strings is expected.', prop);
			end
		else
			switch (prop)
			case 'T'
				ID = setperiod(ID, val);
			case 'output_n'
				if (isempty(val) || (isscalar(val) && isreal(val) && val > 0 && floor(val) == val))
					ID.sink_n = val;
				else
					error ('IdentDevice: set: %s : positive scalar integer or empty matrix is expected.', prop);
				end
			case 'output_dim'
				if (val == [] || val == 1 || val == 2) 
					ID.sink_dim = val;
				else
					error ('IdentDevice: set %s : 1 or 2 is expected.', prop)
				end
			case 'io_mode'
				switch (val) 
				case 'decimal'
					send(ID, 'mod d');
					recv(ID, 'OK')
				case 'raw'
					send(ID, 'mod r');
					recv(ID, 'OK')
				otherwise
					error('IdentDevice: set: invalid io_mode');
				end
			case 'timeout'
				set(ID.dev, 'Timeout', val)
			otherwise
				error ('IdentDevice: set: invalid property <%s> of IdentDevice class', prop);
			end
		end
	end
	out = ID;
end
