% -*- texinfo -*-
%@deftypefn{Function File} {ID = } setperiod (@var{ID}, @var{T})
%Set sampling period to @var{T} (seconds). Equivalent to call @samp{set(ID, 'T', T)}.
%
%@seealso{IdentDevice, set}
%@end deftypefn
function ID = setperiod(ID, T)
	if (isreal(T) && isscalar(T) && T >= 0.0001 && T <= 0.065535)
		send(ID, sprintf('srt %d', round(T * 1e+6)));
		recv(ID, 'OK');
		ID.T = T;
	else
		error('IdentUtil: setperiod: real number between 0.0001 and 0.065 expected.')
	end  
end

