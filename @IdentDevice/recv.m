% -*- texinfo -*-
%@deftypefn {Function File} {[@var{V1}, ... @var{Vn}] =  } recv {(@var{ID},@var{template})}
%Read the next line in device response and parse it accoding to @var{template} 
%string using @code{sscanf} function. @var{n} is number of expected parameters
%parse. These parameters are returned in list @var{V1}, ... @var{Vn}.
%If begining of the line does not match to template throw error. 
%
%Meant to be used in pair with function @code{send}: 
%@example
%send('get %d', 0x1)
%val = recv('%d')
%recv('OK')
%@end example
%
%@seealso{IdentDevice, send} 
%@end deftypefn
function varargout = recv(ID, template)
	str = getline(ID);
	% received = str
	if (nargout == 0) 
		varargout = {};
		if (~ strncmp(str, template, length(template)))
			error('IdentDevice: Device response is <%s>. <%s> expected.', str, template)
		end
	else 
		arr = sscanf(str, template, nargout);
		if (length(arr) ~= nargout) 
			error('IdentDevice: Device response is <%s>. <%s> expected.', str, template)
		end
		varargout = num2cell(arr(:));
	end 
end
