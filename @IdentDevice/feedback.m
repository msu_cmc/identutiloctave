% -*- texinfo -*-
%@deftypefn{Function File} { @var{ID} = } feedback (@var{ID}, @var{REGULATOR}, @var{ARG1}, ... )
%Close feedback loop on device @var{ID} with regulator @var{REGULATOR}. 
%Input, reference and output signals are selected by signal masks (see @code{set}).
%@var{ARG1}, @var{ARG2} are parameters of regulator. 
%
%Note: IdentUtil firmware works with signed 7.8 fixed points, so appropriate conversation is performed.
%
%@table @samp
%@item none
%	Remove regulator.	
%@item openloop	
%	Openloop: control = reference.
%@item pid
%	Add discreet PID regulator with parameters @var{Kp}, @var{Ki}, @var{Kd}. 
%@example
%@group
%        /          z       z-1 \     
% u(z) = | Kp + Ki --- + Kd --- | e(z)
%        \         z-1       z  /     
%@end group
%@end example
%	where @var{u} is control and @var{e} is control error, which is equal to difference between reference and 
%   actual value of signal.
%@item tfz
%	Add discret regulator with given transfer function @var{tf}. Regulator is reduced to form 
%@example
%@group
%                     -1           -m
%            b0 + b1 z  + ... bm  z     
% u(z) =    -------------------------  e(z),
%                     -1          -n
%             1 + a1 z  + ... an z    
%@end group
%@end example
%where coefficients are signed 7.8 fixed points.
%@item relay
%  	Add relay in feedback loop. Parameter @var{K} is magnitude of control.
%	@example
% u =  K sign(e)
%	@end example
%@end table
%@end deftypefn
function out = feedback(ID, regulator, varargin)
	switch  regulator
	case 'none'
		send(ID, 'rno');
		recv(ID, 'OK');
	case 'openloop'
		send(ID, 'rol 0x%x 0x%x', ID.fb_ref_mask, ID.control_mask);
		recv(ID, 'OK');
	case 'pid'
		% convert в 8.8 fixed point
		for k=1:3
			if (~isscalar(varargin{k}) || ~isreal(varargin{k}) || abs(varargin{k}) >= 128) 
			   error('IdentDevice: pid coefficients: real numbers in [-127,127] are expected');
			end
		end
		send(ID, sprintf('rpi %d %d %d 0x%x 0x%x 0x%x', round(varargin{1} * 256), round(varargin{2} * 256), round(varargin{3} * 256), ...
				ID.fb_ref_mask, ID.fb_in_mask, ID.control_mask));
		recv(ID, 'OK');
	case 'tfz'
		R = varargin{1};
		if ~strcmp(class(R), 'tf')
			error('IdentDevice: feedback: transfer function expected')
		end	
		if (get(R, 'Ts') ~= ID.T) 
			error('IdentDevice: feedback: transfer function and device sample rates are not equal')
		end
		num = get(R, 'num'); num = num{1,1};
		den = get(R, 'den'); den = den{1,1};
		if numel(num) > numel(den) 
			error('IdentDevice: feedback: transfer function is not physical')
		end
		% TODO diapazone check, TF normalization: выделить главный коэффициент усиления.
		% normalize tf form
		num = num / den(1);	den = den / den(1);
		% pad numerator with zeros
		num = horzcat(zeros([1 numel(den)-numel(num)]), num);
		if (num(end) == 0)
			den = den(2:end);
		else
			den = [den(2:end) 0];
		end
		% convert to 8.8 fixed point
		num = round(256*num); 
		den = round(256*den);
		% send data
		send(ID, 'rtf %d %d 0x%x 0x%x 0x%x', numel(num) - 1, numel(den), ...
				ID.fb_ref_mask, ID.fb_in_mask, ID.control_mask);
		writeinput(ID, vertcat(num, den));
		recv(ID, 'OK')
	case 'relay'
		if (~isscalar(varargin{1}) || ~isreal(varargin{1})) 
		   error('IdentDevice: relay  coefficients: real numbers in are expected');
		end
		send(ID, 'rrl %d 0x%x 0x%x 0x%x', round(varargin{1}), ...
				ID.fb_ref_mask, ID.fb_in_mask, ID.control_mask);
		recv(ID, 'OK');
	otherwise
		error('IdentDevice: unknown regulator');
	end
	out = ID;
end
