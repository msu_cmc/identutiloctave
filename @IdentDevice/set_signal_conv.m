% -*- texinfo -*-
%@deftypefn{Function File} { @var{ID} = } set_signal_conv (@var{ID}, @var{sig_id}, @var{sig_desc})
%Change signal description. @var{sig_id} is signal index, @var{sig_desc} is signal description in 
%form string or struct. Struct format see in @code{get}. Description string contains fields of the
%strict separated by spaces.
%
%@seealso{IdentDevice, describe, get_signal_conv}
% @end deftypefn
function ID = set_signal_conv(ID, sig_id, sig_desc)
	if (sig_id > ID.state_dim) 
		error('IdentDevice: set_signal_conv: bad signal index')
	end
	if (isstruct(sig_desc))
		ID.signal_desc{sig_id} = sig_desc;
	elseif (isstr(sig_desc))
		ID.signal_desc{sig_id} = parse_signal_desc(sig_desc);
	else
		error('IdentDevice: set_signal_conv: invalid signal description: struct or string expected')
	end
end
		
