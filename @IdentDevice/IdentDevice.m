% -*- texinfo -*-
%@deftypefn {Function File} {@var{id} = } IdentDevice {(@var{port}, @var{baudrate})}
%@deftypefn {Function File} {@var{id} = } IdentDevice {(@var{port}, @var{baudrate}), [@var{propname}, @var{propvalue}])}
%Create IdentDevice object.
%
%The target device is hardware microcontroller board running IdentUtil firmware.
%@var{ID} object allows to interact with IdentUtil firmware via the serial link.
%Serial link is specified by port name @var{PORT}, @var{BAUDRATE}. Open mode is
%8 data bits, none parity and 1 stop bit. Additional @var{propname}, @var{propvalue}
%pairs are passed to underlying @code{serialport()} or @code{serial()} function call.
%
%In following example port is configured for 57600 baurdrate:
%@example
%ID = IdentDevice('/dev/ttyUSB0', 57600)
%@end example
%@end deftypefn
function ID = IdentDevice(device, baudrate, varargin)
	IDs.device = device;
	IDs.baudrate = baudrate;
	if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
		% Octave code
		pkg("load", "instrument-control")
		IDs.dev = serialport(device, 'BaudRate', baudrate, 'Parity', 'none', 'DataBits', 8, 'StopBits', 1);
		configureTerminator(IDs.dev, 'LF')
	else
		%Matlab code
		IDs.dev = serial(device, 'BaudRate', baudrate, 'Parity', 'none', 'DataBits', 8, 'StopBits', 1, 'Terminator', 'LF');
		fopen(IDs.dev);
	end
	%default values
	IDs.success = 0;
	IDs.T = 0.01;
	IDs.source_div = 1;
	IDs.sink_div = 1;
	IDs.sink_delay = 0;
	IDs.sink_n = [];
    IDs.state_dim = 0;
	IDs.signal_desc = {};
	IDs.source_mask = [];
	IDs.sink_mask = [];
	IDs.control_mask = [];
	IDs.fb_ref_mask = [];
	IDs.fb_in_mask = [];
	set(IDs.dev, 'Timeout', 5.0);
	ID = class(IDs, 'IdentDevice');
	% Проверим, что на том конце не верблюд.
	try
		flush(ID);
		pause(0.2);
		send(ID, 'ver');
		recv(ID, 'ident_util 2.1');
		recv(ID, 'OK');
		% Get and print experiment description
		send(ID, 'dex');
		% Get and set default masks
		[ID.state_dim ID.source_mask ID.control_mask ID.sink_mask ] = recv(ID, 'signals: %d %x %x %x');
		ID.fb_in_mask = ID.sink_mask;
		ID.fb_ref_mask = ID.source_mask;
		% Print experiment description line
		str = getline(ID);
		disp(str);
		recv(ID, 'OK');
		% Get and print signal description 
		for k = 1:ID.state_dim
			send(ID, 'dsv %d', k-1);
			str = getline(ID);
			recv(ID, 'OK');
			ID.signal_desc{k} = parse_signal_desc(str);
		end
		% Set default period 
		setperiod(ID, ID.T);
	catch
		delete(ID);
		rethrow(lasterror);
	end 
end
