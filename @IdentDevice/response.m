% -*- texinfo -*-
%@deftypefn{Function File} {[@var{T}, @var{OUT}, @var{INFULL}] = } response (@var{ID}, @var{in})
%Perform experiment. 
%
%Send @var{IN} as input to plant and return its response by @var{OUT}. Each row of @var{OUT} contains one sample.
%Different properties of the experiment can be configured with @code{set} and @code{feedback} functions.
%@var{T} contains timestamps (seconds), @var{OUT} contains device output and @var{INFULL} stores full input 
%signals with all missing values (if @code{input_div} property is not equal to one).
%Accoding to @code{output_div} or @code{output_delay} properties some output values may be missing. Its values are replaced by @code{NaN}.
%
%@seealso{IdentDevice, set, feedback}
%@end deftypefn
function [T, Out, InFull] = response(ID, In)
	if (~ismatrix(In) || size(In,2) ~= sum(bitget(ID.source_mask, 1:8)))
		error('IdentDevice: In: expected matrix with %d columns.', sum(bitget(ID.source_mask, 1:8)));
	end
	flush(ID);
	ID.success = 0;
	% Конфигурируем выход
	if (isempty(ID.sink_n))
		sink_n = floor(size(In,1)*ID.source_div/ID.sink_div);
	else
		sink_n = ID.sink_n;
	end
	send(ID, 'ops %d %d %d 0x%x', sink_n, ID.sink_delay, ID.sink_div, ID.sink_mask);
	recv(ID, 'OK');
	% convert input vector
	In = conv_signal(ID, In, ID.source_mask);
	% Конфигурируем вход 
	send(ID, 'ibs %d %d 0x%x', size(In,1), ID.source_div, ID.source_mask);
	writeinput(ID, In);
	recv(ID, 'OK');
	% Добавляем к входному сигналу значения, упущенные из-за делителя.
	InFull = inputexpanded(ID, In);
	% Проводим эксперимент
	ID.success = 1; 
	send(ID, 'exp');
	Out = readoutput(ID);
	recv(ID, 'OK');
	% convert output signal
	Out = iconv_signal(ID, Out, ID.sink_mask);
	% Доопределяем входной сигнал или обрезаем его.
	if  (size(InFull,1) < size(Out,1))
		InFull = vertcat(InFull, repmat(InFull(size(InFull,1),:), size(Out,1) - size(InFull,1), 1));
	end
	if (size(InFull,1) > size(Out,1)) 
		InFull = InFull(1:size(Out,1),:);
	end
	InFull = iconv_signal(ID, InFull, ID.source_mask);
	% Добавляем значения времени.
	T = linspace(0, ID.T * (size(Out,1)-1), size(Out,1))';
end

	
	
	
		

