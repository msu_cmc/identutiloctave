% -*- texinfo -*-
%@deftypefn{Function File} { @var{sig_desc} = } get_signal_conv (@var{ID}, @var{sig_id})
%Get signal description in the form of structure.
%
%The structure include following fields:
%@table @code
%@item name
%    Signal name. The signal can be addressed by its name.
%@item units
%    Signal measurement units.
%@item coeff
%@itemx offset
%    Conversation parameters: 
%@example
% sig_val_units = coeff * (sig_val_int16 - offset)
%@end example
%@item min
%@itemx max
%    Minimal and maximal value of signal in int16 representation.
%@item range
%    Can be @samp{sat} or @samp{cont}. @samp{sat} means that signal values are always 
%    between @var{min} and @var{max}. Otherwise signal is considered continous and 
%    is being 'wrapped out' accordingly.
%@item type
%    Default type of the signal, they are used to construct default signal masks (see @code{set}).
%    @samp{sink} signals by default transmitted to host from the device (for example, sensors data),
%    @samp{source} signals are transmitted in otherwise direction (reference signal or control),
%    @samp{control} is a output of the regulator (plant input), @samp{none} means no special role.
%@end table
%
%@seealso{IdentDevice, describe, set_signal_conv}
% @end deftypefn
function sig_desc = get_signal_conv(ID, sig_id)
	if (sig_id > ID.state_dim) 
		error('IdentDevice: set_signal_conv: bad signal index')
	end
	sig_desc = ID.signal_desc{sig_id};
end
		
