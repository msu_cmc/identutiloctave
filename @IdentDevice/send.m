% -*- texinfo -*-
%@deftypefn{Function File} {} send (@var{ID}, @var{template}, ...)
%Perform @code{prinft} expansion of @var{template} string and send it to device.
%Send result string to device. 
%
%This function is meant to be used in pair with @code{recv}: 
%@example
%send("srt %d", 10000)
%recv("OK")
%@end example
%
%@seealso{IdentDevice, recv} 
%@end deftypefn
function send(ID, template, varargin)
	str = sprintf(template, varargin{:});
	% sended = str
	if (exist('OCTAVE_VERSION', 'builtin') ~= 0) 
		res = write(ID.dev, horzcat(str, "\n"));
		if ( res ~= (length(str) + 1) )
			error('Unable send string to device: written %d bytes, expected to write %d.', res, length(str)+1)
		end
	else
		fwrite(ID.dev, horzcat(str, "\n"));
	end
end
