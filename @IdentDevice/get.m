% -*- texinfo -*-
%@deftypefn{Function File} { @var{VAL} = } get (@var{ID}, @var{PROP})
%Return the value of the property @var{PROP} associated with device @var{ID}/
%
%The list of available properties see in @code{set}.
%
%@seealso{IdentDevice, set}
%@end deftypefn
function out = get (ID, field)
	if (nargin == 1)
		out = ID;
	elseif (nargin == 2)
		if (ischar (field))
			switch (field)
			case 'dev'
				out = ID.dev;
			case 'T'
				out = ID.T;
			case 'input_div'
				out = ID.source_div;
			case 'output_div'
				out = ID.sink_div;
			case 'output_delay'
				out = ID.sink_delay;
			case 'output_n'
				out = ID.sink_n;
			case 'input_mask'
				out = ID.source_mask; 
			case 'input_names'
				out = sig_names(ID.signal_desc, ID.source_mask); 
			case 'output_mask'
				out = ID.sink_mask;
			case 'output_names'
				out = sig_names(ID.signal_desc, ID.sink_mask);
			case {'control_mask', 'fb_out_mask'}
				out = ID.control_mask;
			case {'control_names', 'fb_out_names'}
				out = sig_names(ID.signal_desc, ID.control_mask);
			case 'fb_ref_mask'
				out = ID.fb_ref_mask 
			case 'fb_ref_names'
				out = sig_names(ID.signal_desc, ID.fb_ref_mask);
			case 'fb_in_mask'
				out = ID.fb_in_mask 
			case 'fb_in_names'
				out = sig_names(ID.signal_desc, ID.fb_in_mask);
			case 'timeout'
				out = get(ID.dev, 'Timeout')
			otherwise
				error ('IdentDevice: get: invalid property %s', field);
			end
		else
			error ('IdentDevice: get: expecting the property to be a string');
		end
	else
		error('IdentDevice: get: invalid number of arguments');
	end
end

