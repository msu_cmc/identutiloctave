% -*- texinfo -*-
%@deftypefn{Function File} {} flush (@var{ID})
%Flush output of target device @var{ID}.
%
%@seealso{IdentDevice}
%@end deftypefn
function flush(ID)
%	send(ID, 'ver');
%	do
%		str = getline(ID)
%	until (strmatch('ident_util', str))
%	recv(ID, 'OK');
	if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
		% Octave code
		flush(ID.dev, 'input');
	else
		% Matlab code
		while true 
			nbytes = get(ID.dev, 'BytesAvailable');
			if (nbytes == 0)
				break;
			end
			fread(ID.dev, nbytes);
		end
	end
end
