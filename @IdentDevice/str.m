function str(ID, pos, in)

    [m n]=size(in);
    if (m ~= 1)
        error('IdentDevaice: str: Arg3 must be 1xN vector');
    end
    send(ID, 'str %d %d', pos, n);
	writeinput(ID, in);
	recv(ID, 'OK');

end
