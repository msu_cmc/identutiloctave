% -*- texinfo -*-
%@deftypefn{Function File} {} delete (@var{ID})
%Destruct IdentDevice object @var{ID} and close serial port connection. 
%
%@seealso{IdentDevice}
%@end deftypefn
function delete(ID)
	if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
		ID.dev = [];
	else
		fclose(ID.dev);
	end
end
