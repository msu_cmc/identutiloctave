% -*- texinfo -*-
%@deftypefn{Function File} {@var{sig_out} = } iconv_signal (@var{ID}, sig_in, mask)
%Convert signal from the sequence of int16 to physical units.
%
%@seealso{parse_signal_desc, conv_signal}
%@end deftypefn
function out_sig = iconv_signal(ID, in_sig, mask)
	SV = ID.signal_desc;
	ind = 1;
	out_sig = [];
	for k = 1:numel(SV)
		if (bitand(mask, bitset(0, k))) 
			in = double(in_sig(:, ind));
			ind = ind + 1;
			out = in;
			if (strcmp(SV{k}.range, 'cont'))
				sig_range = SV{k}.max - SV{k}.min;
				correction = 0;
				for l=2:length(in)
					dy = in(l) - in(l-1);
					if (dy > sig_range/2) 
						correction = correction - sig_range;
					elseif (dy < -sig_range/2)
						correction = correction + sig_range;
					end
					out(l) = out(l) + correction;
				end
			end
			out = SV{k}.coeff * (out - SV{k}.offset);
			out_sig = horzcat(out_sig, out);
		end
	end
end

