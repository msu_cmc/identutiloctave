% -*- texinfo -*-
%@deftypefn{Function File} {[@var{T}, @var{OUT}, @var{INFULL}] = } responseperiodic (@var{ID}, @var{IN}, @var{N_PERIODS})
%Perform experiment on with device @var{ID} with periodic input.
%
%Send periodically expanded vector @var{IN} as input to plant and return its response.
%Argument @var{N_PERIODS} contains the number of periods.
%Different properties of experiment can be configured before by @code{set} and @code{feedback} functions.
%Returns @var{T} (time, seconds), @var{Out} (output, if @code{output_div} or @code{output_delay} is set, 
%missing values are repalced by @code{NaN})
% and  @var{InFull} (real input, expanded according to value of @code{input_div} property). 
%Each row of the input and output contains one data sample.
%
%@seealso{IdentDevice, set, feedback, response}
%@end deftypefn
function [T, Out, InFull] = responseperiodic(ID, In, n_periods)
	if (~isscalar(n_periods) || ~isreal(n_periods) || floor(n_periods) ~= n_periods)
		error('IdentDevice: n_periods: positive scalar integer expected.');
	end
	if (~ismatrix(In) || size(In,2) ~= sum(bitget(ID.source_mask, 1:8)))
		error('IdentDevice: In: expected matrix with %d columns.', sum(bitget(ID.source_mask, 1:8)));
	end
	flush(ID);
	ID.success = 0;
	% Конфигурируем выход
	if (isempty(ID.sink_n))
		sink_n = floor(size(In,1)*ID.source_div*n_periods/ID.sink_div);
	else
		sink_n = ID.sink_n;
		n_periods = ceil( (ID.sink_n*ID.sink_div + ID.sink_delay) / (size(In,1)*ID.source_div) );
	end
	send(ID, 'ops %d %d %d 0x%x', sink_n, ID.sink_delay, ID.sink_div, ID.sink_mask);
	recv(ID, 'OK');
	% Конфигурируем вход 
	% convert input vector
	In = conv_signal(ID, In, ID.source_mask);
	% Вычисляем необходимое число периодов.
	send(ID, 'ibp %d %d %d 0x%x', size(In,1), n_periods, ID.source_div, ID.source_mask);
	writeinput(ID, In);
	recv(ID, 'OK');
	% Добавляем к входному сигналу значения, упущенные из-за делителя.
	In = inputexpanded(ID, In);
	% И переодически его продолжаем.
	InFull = In;
	for k = 2:n_periods
		InFull = vertcat(In, InFull);
	end
	% Проводим эксперимент
	ID.success = 1;
	send(ID, 'exp');
	Out = readoutput(ID);
	recv(ID, 'OK');
	% convert outout signal
	Out = iconv_signal(ID, Out, ID.sink_mask);
	% Обрезаем входной сигнал, если требуется.
	if (size(InFull,1) > size(Out,1)) 
		InFull = InFull(1:size(Out,1),:);
	end
	InFull = iconv_signal(ID, InFull, ID.source_mask);
	% Добавляем значения времени.
	T = linspace(0, ID.T * (size(Out,1)-1), size(Out,1))';
end

