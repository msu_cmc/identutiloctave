% -*- texinfo -*-
%@deftypefn{Function File} {@var{sig_out} = } conv_signal (@var{ID}, sig_in, mask)
%Convert signal from physical units to sequence of uint16.
%
%Convert signal from physical units to sequence of uint16. The rules of conversation
%are received from IdentUtil firmware during initialization.
%
%@seealso{set_signal_conv, get_signal_conv}
%@end deftypefn
function out_sig = conv_signal (ID, in_sig, mask)
	SV = ID.signal_desc;
	ind = 1;
	out_sig = [];
	for k = 1:numel(SV)
		if (bitand(mask, bitset(0, k))) 
			out = in_sig(:, ind);
			ind = ind + 1;
			out = out / SV{k}.coeff + SV{k}.offset;
			switch SV{k}.range;
			case 'sat'
				if (any(out > SV{k}.max) || any(out < SV{k}.min))
					warning('IdentDevice: conv_signal: input signal is out of range');
				end
				out = max(out, SV{k}.min);
				out = min(out, SV{k}.max);
			case 'cont'
				out = out - SV{k}.min;
				out = mod(out, SV{k}.max - SV{k}.min);
				out = out + SV{k}.min;
			end
			out_sig = horzcat(out_sig, cast(out, 'int16'));
		end
	end
end

