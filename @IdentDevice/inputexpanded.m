% -*- texinfo -*-
%@deftypefn{Function File} {@var{InFull} = } inputexpanded (@var{ID}, @var{In})
%Helper function. Expand input vector @var{In} according to @code{input_div} property settings.  
%@end deftypefn
function InFull = inputexpanded(ID, In)
	InFull = In';
	InFull = repmat(InFull, ID.source_div);
	InFull = reshape(InFull, [size(In,2) ID.source_div*size(In,1)])';
end
