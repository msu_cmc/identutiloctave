% -*- texinfo -*-
%@deftypefn {Function File} {ret = } isOctave (@var{ID})
%Return true if script executed in Octave enviroment.
%@end deftypefn
function ret = isOctave(ID)
	ret = exist('OCTAVE_VERSION', 'var') ~= 0;
end

