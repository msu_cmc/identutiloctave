function [io_mode, n_samples, sample_width] = getiomarker(ID)
	line1 = getline(ID);
	[io_mode, line1]  = strtok(line1);
	[v, count] = sscanf(line1, '%d', 2);
	if (count ~= 2) 
		error('IdentDevice: unable parse IO marker line: %s %s', io_mode, line1)
	end	
	n_samples = v(1); sample_width = v(2);
end


