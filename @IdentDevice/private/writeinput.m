% -*- texinfo -*-
% @deftypefn {Function File} {}  writeinput {(@var{ID}, @var{In})}
% Wait for marker "Start load" then writes signal @var{In} and read marker "End load"
% @end deftypefn
function writeinput(ID, In)
	recv(ID, 'Start load');
	[io_mode, n_samples, sample_width] = getiomarker(ID);
	% check data size
	if (any(size(In) ~= [n_samples sample_width]))
		error('IdentDevice: size mismatch for In array: received %d x %d, expected %d x %d', size(In,1), size(In,2), n_samples, sample_width);
	end 
	% write data
	switch io_mode
	case 'decimal'
		for k = 1:size(In,1)
			%write one data sample
			send(ID, '%d ', In(k,:));
		end
	case 'int16x86'
		% wrap out in row-major order and convert ot uint8 array
		data = cast(reshape(In', [numel(In), 1]), 'int16');
		data = typecast(data, 'uint8');
		% write data
		bytes_to_write = length(data);
		bytes_written = 0;
		while bytes_written < bytes_to_write
			nbytes = write(ID.dev, data(bytes_written+1:end));
			bytes_written = bytes_written + nbytes;
			if nbytes == 0
				error('IdentDevice experiment: write error.');
			end
		end
	otherwise
		error('IdentDevice: unknown IO mode %s', io_mode);
	end
	recv(ID, 'End load');
end
