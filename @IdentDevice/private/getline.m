% -*- texinfo -*-
% @deftypefn{Function File} {@var{line} = } getline (@var{ID})
% Nonblocking version getl(). In Octave getl() hangs when reads serial port.
% @end deftypefn
function line = getline(ID) 
	if (exist('OCTAVE_VERSION', 'builtin') ~= 0) 
		% read data byte by byte because readline() drops everyshing after newline
		line = '';
		while true
			data = read(ID.dev, 1);
			if length(data) != 1
				error('IdentUtil: readline timeout.')
			end
			if data(1) == "\n"
				break 
			end 
			line(end+1) = char(data);
		end
	else
		line = fgetl(ID.dev);
	end
end
