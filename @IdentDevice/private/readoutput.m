% -*- texinfo -*-
% @deftypefn{Function File} {@var{Out} = } readoutput (@var{ID}, @var{n_samples})
% Принимает результаты эксперимента начиная от маркета "Start output", до маркета "End output" в случае 
% текстового в/в, в случае бинарного --- считывает @var{n_samples}.
% Неизвестные значения (задержка начала вывода или выходной делитель) заполняет значением NaN
% @end deftypefn
function Out = readoutput(ID)
	recv(ID, 'Start output');
	[io_mode, n_samples, sample_width] = getiomarker(ID);
	Out = NaN(ID.sink_delay + ID.sink_div * n_samples, sample_width);
	switch io_mode
	case 'decimal'
		cursor = ID.sink_delay + 1;
		while true
			line1 = getline(ID);
			[tmp, count] = sscanf(line1, '%d', sample_width);
			if (count == sample_width) 
				Out(cursor, :) = tmp;
				cursor = cursor + ID.sink_div;
			else
				if (strncmp(line1, 'End output', 10))
					break
				elseif (strncmp(line1, 'Aborted', 7) || strncmp(line1, 'Failed', 6))
					error('IdentDevice experiment: %s', line1)
				else
					error('IdentDevice experiment: bad string is received: %s', line1)
				end
			end
		end
	case 'int16x86'
		bytes_to_receive = n_samples * sample_width * 2;
		data = zeros(bytes_to_receive, 1, 'int8');
		bytes_received = 0;
		while bytes_received < bytes_to_receive
			% read data
			[tmp, nbytes, errmsg] = fread(ID.dev, bytes_to_receive - bytes_received, 'int8');
			if errmsg
				error('IdentDevice experiment: read error: %s', errmsg)
			end
			% copy to buffer
			data(bytes_received+1:bytes_received+nbytes) = tmp;
			bytes_received = bytes_received + nbytes;
			% check if timeout is hit (nothing is read)
			if nbytes == 0
				break;
			end
		end
		% get end maker marker
		if bytes_received == bytes_to_receive
			end_line = getline(ID);
		end
		% check if error occurs
		if (bytes_received < bytes_received) || ~strcmp(end_line, 'End output')
			% convert output ot string and try to figure reason
			data = horzcat(typecast(data', 'char'), end_line);
			for pattern = {'Aborted', 'Failed'}
				pindex = strfind(data, pattern{1});
				if pindex
					error('IdentDevice experiment: %s', data(pindex:end))
				end
			end
			% default error
			error('IdentDevice experiment: incomplete output or timeout (received %d bytes, expected %d bytes).', bytes_received, bytes_to_receive)
		end
		% cast to int16 and copuu to output array
		data = cast(typecast(data, 'int16'), 'double');
		data = reshape(data, [sample_width, n_samples]);
		Out(ID.sink_delay+1:ID.sink_div:end, :) = data';
	otherwise
		error('IdentDevice: unknown IO mode "%s"', io_mode);
	end	
end
