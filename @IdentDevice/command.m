% -*- texinfo -*-
%@deftypefn{Function File} { @var{RESP} = } command (@var{ID}, @var{COMMAND})
%Send string @var{COMMAND} to the target device @var{ID}.
%
%This function sends string @var{COMMAND} to IdentUtil firmware @var{ID}, return its response in @var{RESP}.
%Each row of cell array @var{RESP} contains one line of device response. 
%Function waits for the next line until it gets line which begins from @samp{OK} or @samp{Fail}.
%@samp{OK} means successful execution of command, @samp{Fail} --- failure.
%
%@seealso{IdentDevice}
% @end deftypefn
function resp = command(ID, str)
	resp = [];
	send(ID, str);
	resp = {};
    do
		ln = getline(ID);
		resp{end+1} = ln;
	until (regexp(ln, 'OK|Fail'))
end
